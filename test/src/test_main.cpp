#include <utils/utest.h>
#include "test_list.h"
#include <iostream>

using namespace utils;

int main() {
	if (test_component("FileSystem test", file_system_tests)) return -1;
	if (test_component("UBits test", bits_tests)) return -2;
	if (test_component("Serialization test", serialization_tests)) return -2;
    std::cout << "Tests RUN" << std::endl;
	return 0;
}