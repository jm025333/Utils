#include "test_list.h"
#include <utils/utest.h>
#include <utils/ubits.h>
#include <utils/uexceptions.h>
#include <iostream>
using namespace std;
using namespace utils;

bool test_bits() {
    u8 a = 0b1101;
    throw_assert( !is_bit_high( a, 8 ) && !is_bit_high( a, 4 ) && is_bit_high( a, 3 ) && is_bit_high( a, 2 ) && !is_bit_high( a, 1 )
                  && is_bit_high( a, 0 ),
                  "is_bit_high() on u8 not correct." );
    throw_assert( is_bit_low( a, 8 ) && is_bit_low( a, 4 ) && !is_bit_low( a, 3 ) && !is_bit_low( a, 2 ) && is_bit_low( a, 1 )
                  && !is_bit_low( a, 0 ),
                  "is_bit_low() on u8 not correct." );
                  
    i8 b = 0b10001101;
    cout << "b is " << ( i32 )b << endl;
    throw_assert( !is_bit_high( b, 8 ),
                  "is_bit_high() on i8 not correct." );
    throw_assert( is_bit_high( b, 7 ),
                  "is_bit_high() on i8 not correct." );
    throw_assert( is_bit_high( b, 3 ) && is_bit_high( b, 2 ) && !is_bit_high( b, 1 )
                  && is_bit_high( b, 0 ),
                  "is_bit_high() on i8 not correct." );
    throw_assert( is_bit_low( b, 6 ) && !is_bit_low( b, 7 ) && !is_bit_low( b, 3 ) && !is_bit_low( b, 2 ) && is_bit_low( b, 1 )
                  && !is_bit_low( b, 0 ),
                  "is_bit_low() on i8 not correct." );
                  
    i8 c = -2;
    throw_assert( is_bit_high( c, 7 ),
                  "is_bit_high() on negative i8 not correct." );
                  
    u64 d = 0b100000000000000000000000000000001101;
    throw_assert( !is_bit_high( d, 63 ) && is_bit_high( d, 35 ) && !is_bit_high( d, 36 ) && is_bit_high( d, 3 ) && is_bit_high( d, 2 )
                  && !is_bit_high( d, 1 ) && is_bit_high( d, 0 ),
                  "is_bit_high() on u64 not correct." );
    throw_assert( is_bit_low( d, 63 ) && !is_bit_low( d, 35 ) && is_bit_low( d, 36 ) && !is_bit_low( d, 3 ) && !is_bit_low( d, 2 )
                  && is_bit_low( d, 1 ) && !is_bit_low( d, 0 ),
                  "is_bit_low() on u64 not correct." );
                  
    i64 e = 0b100000000000000000000000000000001101;
    throw_assert( !is_bit_high( e, 63 ) && is_bit_high( e, 35 ) && !is_bit_high( e, 36 ) && is_bit_high( e, 3 ) && is_bit_high( e, 2 )
                  && !is_bit_high( e, 1 ) && is_bit_high( e, 0 ),
                  "is_bit_high() on i64 not correct." );
    throw_assert( is_bit_low( e, 63 ) && !is_bit_low( e, 35 ) && is_bit_low( e, 36 ) && !is_bit_low( e, 3 ) && !is_bit_low( e, 2 )
                  && is_bit_low( e, 1 ) && !is_bit_low( e, 0 ),
                  "is_bit_low() on i64 not correct." );
                  
    i64 f = -2;
    throw_assert( is_bit_high( f, 63 ),
                  "is_bit_high() on negative i64 not correct." );
    return false;
}


void bits_tests(bool& flag) {
	test("Various Bit tests", test_bits, flag);
}