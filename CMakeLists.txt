cmake_minimum_required(VERSION 3.15)

# OPTIONS:

option(Utils_USE_FILESYSTEM "Whether to include the 'ufilesystem' module." ON)
option(Utils_USE_EXCEPTIONS "Whether to include the 'uexceptions' module." ON)
option(Utils_USE_STREAM "Whether to include the 'ustream' module." ON)
option(Utils_USE_CONSOLE "Whether to include the 'uconsole' module." ON)
option(Utils_USE_TEST "Whether to include the 'utest' module." ON)
option(Utils_USE_SERIALIZATION "Whether to include the 'userialization' module." OFF)


project(Utils VERSION 1.0 DESCRIPTION "Basic Utility Library" LANGUAGES CXX)

if (${Utils_USE_TEST})
    set(Utils_USE_STREAM TRUE)
endif()
if (${Utils_USE_FILESYSTEM})
    set(Utils_USE_EXCEPTIONS TRUE)
endif()
if (${Utils_USE_STREAM})
    set(Utils_USE_CONSOLE TRUE)
endif()
if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    set(Utils_USE_SERIALIZATION ON) # Include all modules when building here -> allow testing of all
endif()

# Source and Header files

list(APPEND Utils_HEADERFILES
    include/base/utils/utypes.h
    include/base/utils/ubits.h
    include/base/utils/umath.h
    include/base/utils/ustring.h
    include/base/utils/utime.h
)

list(APPEND Utils_SOURCEFILES
    src/ulib.cpp
)

if (${Utils_USE_FILESYSTEM})
    list(APPEND Utils_HEADERFILES include/filesystem/utils/ufilesystem.h)
    list(APPEND Utils_SOURCEFILES src/filesystem.cpp)
endif()

if (${Utils_USE_EXCEPTIONS})
    list(APPEND Utils_HEADERFILES include/exceptions/utils/uexceptions.h)
    list(APPEND Utils_SOURCEFILES src/exceptions.cpp)
endif()

if (${Utils_USE_STREAM})
    list(APPEND Utils_HEADERFILES include/stream/utils/ustream.h)
    list(APPEND Utils_SOURCEFILES src/stream.cpp)
endif()

if (${Utils_USE_CONSOLE})
    list(APPEND Utils_HEADERFILES include/console/utils/uconsole.h)
    list(APPEND Utils_SOURCEFILES src/console.cpp)
endif()

if (${Utils_USE_SERIALIZATION})
    list(APPEND Utils_HEADERFILES include/serialization/utils/userialization.h)
    list(APPEND Utils_SOURCEFILES src/serialization.cpp)
endif()




# Library declaration

add_library(utils STATIC ${Utils_SOURCEFILES} ${Utils_HEADERFILES})


# Include directories

target_include_directories(utils PUBLIC include/base)
if (${Utils_USE_FILESYSTEM})
    target_include_directories(utils PUBLIC include/filesystem)
    #target_link_libraries(utils PUBLIC Shlwapi.lib)
endif()
if (${Utils_USE_EXCEPTIONS})
    target_include_directories(utils PUBLIC include/exceptions)
endif()
if (${Utils_USE_STREAM})
    target_include_directories(utils PUBLIC include/stream)
endif()
if (${Utils_USE_CONSOLE})
    target_include_directories(utils PUBLIC include/console)
endif()
if (${Utils_USE_SERIALIZATION})
    target_include_directories(utils PUBLIC include/serialization)
endif()


# Compiler options

target_compile_features(utils PUBLIC cxx_std_17)
# target_compile_definitions(utils PUBLIC _MBCS)

# target_compile_options(${PROJECT_NAME} PRIVATE "/MT$<$<CONFIG:Debug>:d>")

# add_definitions(
#     /std:c++17
#     /D_MBCS
#     /permissive-
# )

# UTest

if (${Utils_USE_TEST})
    add_library(utest STATIC src/test.cpp include/test/utils/utest.h)
    target_include_directories(utest PUBLIC include/test)
    target_link_libraries(utest PUBLIC utils)
endif()


# Test Project

if(CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    #include(CTest)
    enable_testing()
    add_subdirectory(test)
endif()





source_group(
    "utils" FILES
    include/base/utils/utypes.h
    include/base/utils/ubits.h
    include/base/utils/umath.h
    include/base/utils/ustring.h
    include/base/utils/utime.h
    src/utils/ulib.cpp
)
source_group(
    "uconsole" FILES
    include/console/utils/uconsole.h
    src/console/console.cpp
)
source_group(
    "ustream" FILES
    include/stream/utils/ustream.h
    src/stream/stream.cpp
)
source_group(
    "ufilesystem" FILES
    include/filesystem/utils/ufilesystem.h
    src/filesystem/filesystem.cpp
)
source_group(
    "uexception" FILES
    include/exceptions/utils/uexceptions.h
    src/exceptions/exceptions.cpp
)