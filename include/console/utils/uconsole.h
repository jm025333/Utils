#ifndef H_UTILS_CONSOLE_INCLUDE
#define H_UTILS_CONSOLE_INCLUDE

namespace utils {

#if defined _WIN32 || defined _WIN64
struct ConsoleColor {
    enum ColorValue {
        BLACK = 0,
        DARK_BLUE = 1,
        DARK_GREEN = 2,
        TURQUOISE = 3,
        DARK_RED = 4,
        PURPLE = 5,
        DARK_YELLOW = 6,
        LIGHT_GRAY = 7,
        DARK_GRAY = 8,
        BLUE = 9,
        GREEN = 10,
        LIGHT_BLUE = 11,
        RED = 12,
        PINK = 13,
        YELLOW = 14,
        WHITE = 15,
        DEFAULT = 7
    };
    ColorValue text_color;
    ColorValue bg_color;
    ConsoleColor( ColorValue text_color = DEFAULT, ColorValue bg_color = BLACK ) : text_color( text_color ), bg_color( bg_color ) {}
    int get() {
        return text_color + ( bg_color * 16 );
    }
    bool operator!=( const ConsoleColor &other ) {
        return text_color != other.text_color || bg_color != other.bg_color;
    }
};


#else
struct ConsoleColor {
    enum ColorValue {
        BLACK		= 30,
        DARK_RED	= 31,
        DARK_GREEN	= 32,
        DARK_YELLOW = 33,
        DARK_BLUE	= 34,
        PURPLE		= 35,
        TURQUOISE	= 36,
        LIGHT_GRAY	= 37,
        DARK_GRAY	= 90,
        RED			= 91,
        GREEN		= 92,
        YELLOW		= 93,
        BLUE		= 94,
        PINK		= 95,
        LIGHT_BLUE	= 96,
        WHITE		= 97,
        DEFAULT		= 37
    };
    ColorValue text_color;
    ColorValue bg_color;
    ConsoleColor( ColorValue text_color = DEFAULT, ColorValue bg_color = BLACK ) : text_color( text_color ), bg_color( bg_color ) {}
    bool operator!=( const Color &other ) {
        return text_color != other.text_color || bg_color != other.bg_color;
    }
};
#endif



struct Console {
    ConsoleColor current_color;
    

    public:
        Console();
        ~Console();
        
        void set_color( ConsoleColor which );
        inline void set_color( ConsoleColor::ColorValue which ){
			set_color( ConsoleColor( which ) );
		}
        
        void test_color();
        
        
#if defined _WIN32 || defined _WIN64
        void *hstdout;
        unsigned char csbi[22];
#endif

};

extern Console uconsole;

}

#endif