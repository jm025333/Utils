#ifndef H_UTILS_FILESYSTEM_INCLUDE
#define H_UTILS_FILESYSTEM_INCLUDE

#include <string>
#include <vector>
#include <fstream>
#include <filesystem>
namespace fs = std::filesystem;

#include "utils/utypes.h"

namespace file_reader {
    // Returns true on success
    bool read(const fs::path &file, std::vector<utils::u8> &target, bool include_eof = true);
    // Returns true on success
    bool read(const fs::path& file, std::vector<char>& target, bool include_eof = true);
    std::string read_if_exists(const fs::path& path, bool include_eof = true);
}

#endif