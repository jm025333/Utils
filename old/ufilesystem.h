#ifndef H_UTILS_FILESYSTEM_INCLUDE
#define H_UTILS_FILESYSTEM_INCLUDE

#include <string>
#include <vector>
#include <fstream>
#include "utils/utypes.h"
#include "utils/uexceptions.h"

struct FileReader;
struct Library;

/*
    Replacement of some FileSystem functionnalities for compatibility.
*/
namespace FS {

    struct DirectoryContent;
    struct File;
    /*
        Represents a directory (NOT A FILE).
        Only a relative directory can be appended (+ operator) to another directory.
        get_name() returns the name of the directory.
        to_string() returns the path of the directory.
        get_contents() / get_files() / get_directories() list the contents of the directory.
    */
    struct Directory {
        Directory(const std::string& path);
        Directory() : path("") {}
        bool is_absolute() const;
        Directory canonical() const;
        bool exists() const;
        bool mkdir() const;
        DirectoryContent get_contents() const;
        std::vector<File> get_files() const;
        std::vector<Directory> get_directories() const;
        std::string get_name() const;
        std::string to_string() const;
        std::string as_system_path() const;
    private:
        const std::string& get_path() const {
            return path;
        }
        std::string path;
        friend Directory operator+(const Directory& folder, const Directory& folder2);
        friend void fill_directories_files(const Directory& directory, std::vector<Directory>* directories, std::vector<File>* files);
        friend struct File;
        friend struct FileSystemTests;
    };

    /*
        Represents a File Path.
        The path of the file is separated into the "folder" Directory.
        get_full_name() returns the file name with extension.
        get_name() returns the file name without extension.
        to_string() returns the generic file path (not system specific, using '/' delimiter).
    */
    struct File {
        Directory folder;
        File(const Directory& folder, const std::string& name);
        File(const std::string& folder, const std::string& name);
        File(const std::string& name);
        File() : ext_pos(0) {}
        const std::string& get_full_name() const {
            return name;
        }
        std::string get_extension() const {
            return name.substr(ext_pos, std::string::npos);
        }
        std::string get_name() const {
            return name.substr(0, ext_pos);
        }
        bool exists() const;

        std::string to_string() const;
        std::string as_system_path() const;
    private:
        std::string name;
        utils::u32 ext_pos;
        friend File operator+(const Directory& folder, const File& file);
        friend struct ::FileReader;
        friend struct ::Library;
    };

    struct DirectoryContent {
        //Files and Directories are relative to the containing directory
        std::vector<File> files;
        std::vector<Directory> directories;
    };

    File operator+(const Directory& folder, const File& file);
    Directory operator+(const Directory& folder, const Directory& folder2);

    Directory current_directory();



}

/*
    The FileReader struct provides helper functions to open a file, verify that it was correctly opened and dump its content into an
    Array object.
    The file is closed once the content has been read to the array.
    The read() function initiates the array with the size of the file and fills it with its content.
*/
struct FileReader {
    std::ifstream       file;
    bool open(const FS::File& file_path) {
        file = std::ifstream(file_path.as_system_path(), std::ios::in | std::ios::binary | std::ios::ate );
        return file.is_open();
    }
    
    void read( std::vector<char> &target, bool include_eof ) {
        auto pos = file.tellg();
        utils::u32 size = ( utils::u32 )pos;
        target.resize( size + (include_eof ? 1LL:0) );
        file.seekg( 0, std::ios::beg );
        file.read( target.data(), size );
        file.close();
        if (include_eof) target[size] = '\0';
    }
    static std::string read_if_exists(const FS::File& file_path) {
        std::ifstream file = std::ifstream(file_path.as_system_path(), std::ios::in | std::ios::binary | std::ios::ate);
        if (!file.is_open()) //throw_system_error("Could not open file: " + file_path.to_string());
            return std::string();
        auto pos = file.tellg();
        utils::u32 size = (utils::u32)pos;
        std::vector<char> mem(size + 1L);
        file.seekg( 0, std::ios::beg );
        file.read( mem.data(), size );
        mem[size] = '\0';
        std::string res(mem.data());
        return res;
    }
    
    void read( std::vector<utils::u8> &target, bool include_eof ) {
        auto pos = file.tellg();
        utils::u32 size = ( utils::u32 )pos;
        target.resize( size + (include_eof ? 1LL:0) );
        file.seekg( 0, std::ios::beg );
        file.read( ( char * )target.data(), size );
        file.close();
        if (include_eof) target[size] = '\0';
    }
};

#endif